#!/bin/bash

# Install your keys
mkdir -p ~/.ssh
curl https://github.com/jverghese.keys >> ~/.ssh/authorized_keys

# Install nvm: node-version manager
# https://github.com/creationix/nvm
sudo apt-get install -y git-core
curl https://raw.github.com/creationix/nvm/master/install.sh | sh

# Load nvm and install latest production node
source $HOME/.nvm/nvm.sh
nvm install v0.10.12
nvm use v0.10.12

# git pull and install dotfiles as well
ln -sb dotfiles/.screenrc .
ln -sb dotfiles/.bash_profile .
ln -sb dotfiles/.bashrc .
ln -sb dotfiles/.bashrc_custom .
ln -sf dotfiles/.emacs.d .

ssh-keygen -t rsa

# MAC
# Install Iterm
# Install solarized dark theme: https://github.com/altercation/solarized
