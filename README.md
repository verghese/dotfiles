# Vim
spf13
Setup dotfiles and setup: https://github.com/startup-class
Add bash eternal history.
https://github.com/thoughtbot/dotfiles

Todo:
Add z completion script.

Dotfiles
=========
```sh
cd $HOME
sudo apt-get install -y git-core
git clone https://verghese@bitbucket.org/verghese/dotfiles.git
./setup/setup.sh   
```
